<?php

namespace app\controllers;

use mdm\admin\components\UserStatus;
use mdm\admin\models\form\Login;
use mdm\admin\models\form\PasswordResetRequest;
use mdm\admin\models\form\ResetPassword;
use app\models\search\UserSearch;
use app\models\ChangePassword;
use app\models\Signup;
use app\models\RegistrasiAkun;
use app\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\base\UserException;
use yii\filters\VerbFilter;
use yii\mail\BaseMailer;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\MajelisPengawasDaerah;
use app\models\MajelisPengawasWilayah;
use app\models\Notaris;
use yii\web\UploadedFile;

/**
 * User controller
 */
class UserController extends Controller
{
    private $_oldMailPath;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'logout' => ['post'],
                    'activate' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (Yii::$app->has('mailer') && ($mailer = Yii::$app->getMailer()) instanceof BaseMailer) {
                /* @var $mailer BaseMailer */
                $this->_oldMailPath = $mailer->getViewPath();
                $mailer->setViewPath('@mdm/admin/mail');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        if ($this->_oldMailPath !== null) {
            Yii::$app->getMailer()->setViewPath($this->_oldMailPath);
        }
        return parent::afterAction($action, $result);
    }

    public function actionChangePassword()
    {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if($model->change()){
                Yii::$app->session->setFlash('success', "Berhasil ganti password");
                return $this->render('change-password', [
                    'model' => $model,
                ]);
                // return $this->redirect('?r=user/change-password');
            }else{
                Yii::$app->session->setFlash('error', "Gagal ganti password");
                return $this->render('change-password', [
                    'model' => $model,
                ]);
                // return $this->redirect('?r=user/change-password');
            }
            
            // return $this->goHome();
        }

        return $this->render('change-password', [
            'model' => $model,
        ]);
    }

    public function actions()
    {
        return [
            'uploadPhoto' => [
                'class'=> 'budyaga\cropper\actions\UploadAction',
                'url'  => '../media/foto',
                'path' => '../media/foto',
                // 'url' => 'http://localhost/lauk_pauk/media/uploads',
                // 'path' => '@frontend/web/uploads/user/photo',
            ]
        ];
    }
}
