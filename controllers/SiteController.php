<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Informasi;
use app\models\searchs\InformasiSearch;
use yii\helpers\Html;
use app\models\Foto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'access' => [
    //             'class' => AccessControl::className(),
    //             'only' => ['logout'],
    //             'rules' => [
    //                 [
    //                     'actions' => ['logout'],
    //                     'allow' => true,
    //                     'roles' => ['@'],
    //                 ],
    //             ],
    //         ],
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'logout' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    public static function allowedDomains()
    {
        return [
            // '*',                        // star allows all domains
            'http://localhost',
            'http://test2.example.com',
        ];
    }  



    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter'  => [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => static::allowedDomains(),
                    'Access-Control-Request-Method'    => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)

                ],
            ],

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {	
        $model = new Informasi;
		$searchModel = new InformasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    public function actionTampilFotoDaerah($id)
    {
        $datafoto = Foto::find()->where(['idinformasi'=>$id])->all();
        // print_r(count($datafoto));
        $urlfoto = "http://localhost/admin-gis-tanah-karo/web/";
        $data = "";
        echo '<div class="swiper-slide"><img src="../media/foto/622037ba5c651.jpeg" class="img-fluid" alt=""></div>';
        for ($i=0; $i < count($datafoto); $i++) { 
            // echo $i;
            $data = $data.' <div class="swiper-slide"><img src="'.$urlfoto.$datafoto[$i]['nama_foto'].'" alt="" width="500" height="200" class="img-fluid"></div>';
            
        }


        // $data = '
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-1.png" class="img-fluid" alt=""></div>
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-2.png" class="img-fluid" alt=""></div>
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-3.png" class="img-fluid" alt=""></div>
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-4.png" class="img-fluid" alt=""></div>
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-5.png" class="img-fluid" alt=""></div>
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-6.png" class="img-fluid" alt=""></div>
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-7.png" class="img-fluid" alt=""></div>
        // <div class="swiper-slide"><img src="'.Yii::$app->request->baseUrl.'/template/assets/img/clients/client-8.png" class="img-fluid" alt=""></div>
        // ';
        return $data;
    }

    public function actionOpenMap()
    {	
        $model = new Informasi;
		$searchModel = new InformasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderPartial('map', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            // 'model' => $model,
        ]);
    }

    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Detail Informasi",
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Tutup',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Ubah',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Informasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {	
	$model = new LoginForm();
		Yii::$app->user->logout();
        //return $this->render('login', ['model'=>$model]);
        return $this->redirect('?r=site/login');

		
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionKirimemail(){

        $vpass = "sqs123456";
        $model = User::findOne(['email' => $_GET['email']]);
        $model->generateAuthKey();
        $model->setPassword($vpass);
        $model->save();

        //$dataemail = User::findOne(['email' => $_GET['email']]);

        $kirim = Yii::$app->mailer->compose()
        ->setFrom('support_tapem@pematangsiantarkota.com')
        ->setTo($_GET['email'])
        ->setSubject('Reset Password')
        ->setTextBody('Plain text content')
        ->setHtmlBody('<b>Password Baru: '.$vpass.'</b>')
        ->send();

        if($kirim){
            $pesan = array('pesan'=>'Password berhasil direset, Silahkan cek email Anda');
        }else{
            $pesan = array('pesan'=>'Proses gagal');
        }
        //$pesan = array('pesan'=>$dataemail->password_hash);
        echo json_encode($pesan);
    }
}
