<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Video */
?>
<div class="video-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'judul',
            'url:ntext',
            'url_image:url',
            'deskripsi:ntext',
        ],
    ]) ?>

</div>
