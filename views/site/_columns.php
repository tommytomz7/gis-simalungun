<?php
use yii\helpers\Url;
use app\models\Kategori;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    // [
    //     'class' => 'kartik\grid\SerialColumn',
    //     'width' => '30px',
    // ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'idkategori',
    //     'filter'=>ArrayHelper::map(Kategori::find()->all(),'id','nama_kategori'),
    //     'filterType' => GridView::FILTER_SELECT2,
    //     'filterWidgetOptions' => [
    //         'options' => ['placeholder' => 'Pilih'],
    //         'pluginOptions' => [
    //             'allowClear' => true,
    //         ],
    //     ],
    //     'label'=>'Kategori',
    //     'value'=>'kategori.nama_kategori',
    //     'visible' => true,
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'judul',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'Deskripsi',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'lat',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'long',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'header'=>'Aksi',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{koordinat}',
        'buttons' => [
            'koordinat' => function($url, $model, $key) {     // render your custom button
                return Html::a('<span class="glyphicon glyphicon-list btn btn-info btn-xs rounded"></span>', '#about', [
                    // 'options' => array(  // set all kind of html options in here
                        'onclick' =>"
                            $('#deskripsi').html('".$model['deskripsi']."');
                            // $('#map').load('https://www.google.com/maps/dir/?api=1&origin=3.5724223079050144,98.59026091299226&destination=".$model['lat'].",".$model['long']."');
                            // $('#map').load('https://stackoverflow.com/questions/20067027/how-to-pass-headers-in-jquery-load-like-ajax');
                            // const directionsService = new google.maps.DirectionsService();
                            // const directionsRenderer = new google.maps.DirectionsRenderer();

                            // calculateAndDisplayRoute(directionsService, directionsRenderer);
                            document.getElementsByName('framemap')[0].src = 'https://maps.google.com/maps?q=".$model['lat'].",".$model['long']."&t=&z=14&ie=UTF8&iwloc=&output=embed';
                            $('#loadimage').load('".Yii::$app->request->baseUrl."?r=site/tampil-foto-daerah&id=".$model['id']."');
                        ",
                         'style' => 'font-weight: bold',
                    // ),
                    // 'title' => Yii::t('app', 'Foto Informasi'),
                    // 'role'=>'modal-remote',
                    // 'data-toggle'=>'tooltip'
                ]);
            }
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        // 'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        // 'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
        //                   'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
        //                   'data-request-method'=>'post',
        //                   'data-toggle'=>'tooltip',
        //                   'data-confirm-title'=>'Anda Yakin?',
        //                   'data-confirm-message'=>'Apakah ingin menghapus data ini'], 

    ],

];   