<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    // $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title>Wisata Tanah Karo</title>
        <?php $this->head() ?>
        <!-- <link rel="shortcut icon" href="<?=Yii::$app->request->baseUrl?>/images/logo_upu50.png"> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Vendor CSS Files -->
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/aos/aos.css" rel="stylesheet">
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="<?=Yii::$app->request->baseUrl?>/template/assets/css/style.css" rel="stylesheet">
        

    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            // ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'content.php',
            ['content' => $content]
        ) ?>

    </div>

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/purecounter/purecounter.js"></script>
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/aos/aos.js"></script>
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="<?=Yii::$app->request->baseUrl?>/template/assets/js/main.js"></script>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
