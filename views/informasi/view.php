<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Informasi */
?>
<div class="informasi-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'idkategori',
            'Judul',
            'Deskripsi:ntext',
            'lat',
            'long',
        ],
    ]) ?>

</div>
