<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Informasi */

?>
<div class="informasi-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
