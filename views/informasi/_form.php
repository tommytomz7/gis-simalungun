<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Kategori;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\editors\Summernote;

/* @var $this yii\web\View */
/* @var $model app\models\Informasi */
/* @var $form yii\widgets\ActiveForm */

$datakategori = ArrayHelper::map(Kategori::find()->all(),'id','nama_kategori');
?>

<div class="informasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idkategori')->widget(Select2::classname(), [
        'data' => $datakategori,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]); ?>

    <?= $form->field($model, 'Judul')->textInput(['maxlength' => true]) ?>

    <div id="map" style="width:100%;height:400px"></div>

    <?= $form->field($model, 'lat')->textInput() ?>

    <?= $form->field($model, 'long')->textInput() ?>

    <?= $form->field($model, 'Deskripsi')->textInput() ?> 

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<script>
    let markers = [];

    function initMap() {
        
        const myLatlng = { lat: 3.5917701, lng: 98.6789102 };

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 17,
            center: myLatlng,

        });

        // Create the initial InfoWindow.
        // let infoWindow = new google.maps.InfoWindow({
        //     // content: "Click the map to get Lat/Lng!",
        //     position: myLatlng,
        // });

        // infoWindow.open(map);
        // Configure the click listener.
        map.addListener("click", (mapsMouseEvent) => {
            let datalatlng = mapsMouseEvent.latLng.toJSON();
            // document.getElementById('informasi-lat').value = datalatlng.lat;
            // alert(datalatlng.lat);
            setMapOnAll(null);
            markers = [];

            const marker = new google.maps.Marker({
                position: mapsMouseEvent.latLng,
                map,
            });

            markers.push(marker);
            setMapOnAll(map);

            
            document.getElementById('informasi-lat').value= datalatlng.lat;
            document.getElementById('informasi-long').value= datalatlng.lng;
                        
            // Close the current InfoWindow.
            // infoWindow.close();

            
            // ;
            // Create a new InfoWindow.
            // infoWindow = new google.maps.InfoWindow({
            // position: mapsMouseEvent.latLng,
            // });
            // infoWindow.setContent(
            // JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
            // );
            // infoWindow.open(map);
        });
    }

    function addMarker(position) {
        const marker = new google.maps.Marker({
            position: position,
            map,
        });

        markers.push(marker);
    }

    function setMapOnAll(map) {
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }
    
</script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyANXK2dL-zep8x8sobVoLHLG7CAzAbIy5Y&callback=initMap" async defer></script>
            <!-- <script src=“http://somemapprodcutaddress.js” key="AIzaSyANXK2dL-zep8x8sobVoLHLG7CAzAbIy5Y"> </script> -->
