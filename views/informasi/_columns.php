<?php
use yii\helpers\Url;
use app\models\Kategori;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'idkategori',
        'filter'=>ArrayHelper::map(Kategori::find()->all(),'id','nama_kategori'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['placeholder' => 'Pilih'],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'label'=>'Kategori',
        'value'=>'kategori.nama_kategori',
        'visible' => true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Judul',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Deskripsi',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'lat',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'long',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{view} {update} {delete} {foto}',
        'buttons' => [
            'foto' => function($url, $model, $key) {     // render your custom button
                return Html::a('<span class="glyphicon glyphicon-camera btn btn-info btn-xs rounded"></span>', '?r=foto/index&id='.$model->id, [
                    // 'title' => Yii::t('app', 'Foto Informasi'),
                    // 'role'=>'modal-remote',
                    // 'data-toggle'=>'tooltip'
                ]);
            }
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Anda Yakin?',
                          'data-confirm-message'=>'Apakah ingin menghapus data ini'], 

    ],

];   