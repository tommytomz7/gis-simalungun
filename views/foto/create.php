<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Foto */

?>
<div class="foto-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id
    ]) ?>
</div>
