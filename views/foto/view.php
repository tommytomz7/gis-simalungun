<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Foto */
?>
<div class="foto-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'informasi.Judul',
            [
                'attribute'=>'nama_foto',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img($data['nama_foto'],['width' => '150px']);

                },
            ]
            // 'nama_foto',
        ],
    ]) ?>

</div>
