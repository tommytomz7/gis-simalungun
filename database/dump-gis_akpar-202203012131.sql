-- MySQL dump 10.13  Distrib 5.7.34, for osx11.0 (x86_64)
--
-- Host: localhost    Database: gis_akpar
-- ------------------------------------------------------
-- Server version	5.7.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('Admin','47',1643693345);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  KEY `rule_name` (`rule_name`) USING BTREE,
  KEY `idx-auth_item-type` (`type`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/assignment/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/assignment/assign',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/assignment/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/assignment/revoke',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/assignment/view',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/default/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/default/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/menu/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/menu/create',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/menu/delete',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/menu/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/menu/update',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/menu/view',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/assign',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/create',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/delete',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/get-users',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/remove',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/update',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/permission/view',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/assign',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/create',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/delete',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/get-users',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/remove',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/update',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/role/view',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/route/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/route/assign',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/route/create',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/route/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/route/refresh',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/route/remove',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/rule/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/rule/create',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/rule/delete',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/rule/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/rule/update',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/rule/view',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/*',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/activate',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/change-password',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/delete',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/index',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/login',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/logout',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/request-password-reset',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/reset-password',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/signup',2,NULL,NULL,NULL,1643693203,1643693203),('/admin/user/view',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/*',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/default/*',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/default/db-explain',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/default/download-mail',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/default/index',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/default/toolbar',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/default/view',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/user/*',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/user/reset-identity',2,NULL,NULL,NULL,1643693203,1643693203),('/debug/user/set-identity',2,NULL,NULL,NULL,1643693203,1643693203),('/foto/*',2,NULL,NULL,NULL,1643897190,1643897190),('/foto/bulk-delete',2,NULL,NULL,NULL,1643897190,1643897190),('/foto/create',2,NULL,NULL,NULL,1643897190,1643897190),('/foto/delete',2,NULL,NULL,NULL,1643897190,1643897190),('/foto/index',2,NULL,NULL,NULL,1643897190,1643897190),('/foto/update',2,NULL,NULL,NULL,1643897190,1643897190),('/foto/view',2,NULL,NULL,NULL,1643897190,1643897190),('/gii/*',2,NULL,NULL,NULL,1643693203,1643693203),('/gii/default/*',2,NULL,NULL,NULL,1643693203,1643693203),('/gii/default/action',2,NULL,NULL,NULL,1643693203,1643693203),('/gii/default/diff',2,NULL,NULL,NULL,1643693203,1643693203),('/gii/default/index',2,NULL,NULL,NULL,1643693203,1643693203),('/gii/default/preview',2,NULL,NULL,NULL,1643693203,1643693203),('/gii/default/view',2,NULL,NULL,NULL,1643693203,1643693203),('/gridview/*',2,NULL,NULL,NULL,1643693203,1643693203),('/gridview/export/*',2,NULL,NULL,NULL,1643693203,1643693203),('/gridview/export/download',2,NULL,NULL,NULL,1643693203,1643693203),('/informasi/*',2,NULL,NULL,NULL,1643696324,1643696324),('/informasi/bulk-delete',2,NULL,NULL,NULL,1643696324,1643696324),('/informasi/create',2,NULL,NULL,NULL,1643696324,1643696324),('/informasi/delete',2,NULL,NULL,NULL,1643696324,1643696324),('/informasi/index',2,NULL,NULL,NULL,1643696324,1643696324),('/informasi/update',2,NULL,NULL,NULL,1643696324,1643696324),('/informasi/view',2,NULL,NULL,NULL,1643696324,1643696324),('/kategori/*',2,NULL,NULL,NULL,1643693203,1643693203),('/kategori/bulk-delete',2,NULL,NULL,NULL,1643693203,1643693203),('/kategori/create',2,NULL,NULL,NULL,1643693203,1643693203),('/kategori/delete',2,NULL,NULL,NULL,1643693203,1643693203),('/kategori/index',2,NULL,NULL,NULL,1643693203,1643693203),('/kategori/update',2,NULL,NULL,NULL,1643693203,1643693203),('/kategori/view',2,NULL,NULL,NULL,1643693203,1643693203),('/site/*',2,NULL,NULL,NULL,1643693203,1643693203),('/site/about',2,NULL,NULL,NULL,1643693203,1643693203),('/site/captcha',2,NULL,NULL,NULL,1643693203,1643693203),('/site/contact',2,NULL,NULL,NULL,1643693203,1643693203),('/site/error',2,NULL,NULL,NULL,1643693203,1643693203),('/site/index',2,NULL,NULL,NULL,1643693203,1643693203),('/site/kirimemail',2,NULL,NULL,NULL,1643693203,1643693203),('/site/login',2,NULL,NULL,NULL,1643693203,1643693203),('/site/logout',2,NULL,NULL,NULL,1643693203,1643693203),('/tentang/*',2,NULL,NULL,NULL,1645449398,1645449398),('/tentang/bulk-delete',2,NULL,NULL,NULL,1645449398,1645449398),('/tentang/create',2,NULL,NULL,NULL,1645449398,1645449398),('/tentang/delete',2,NULL,NULL,NULL,1645449398,1645449398),('/tentang/index',2,NULL,NULL,NULL,1645449398,1645449398),('/tentang/update',2,NULL,NULL,NULL,1645449398,1645449398),('/tentang/view',2,NULL,NULL,NULL,1645449398,1645449398),('/user/*',2,NULL,NULL,NULL,1643693203,1643693203),('/user/change-password',2,NULL,NULL,NULL,1643693203,1643693203),('/user/uploadPhoto',2,NULL,NULL,NULL,1644075983,1644075983),('/video/*',2,NULL,NULL,NULL,1644139988,1644139988),('/video/bulk-delete',2,NULL,NULL,NULL,1644139988,1644139988),('/video/create',2,NULL,NULL,NULL,1644139988,1644139988),('/video/delete',2,NULL,NULL,NULL,1644139988,1644139988),('/video/index',2,NULL,NULL,NULL,1644139988,1644139988),('/video/update',2,NULL,NULL,NULL,1644139988,1644139988),('/video/view',2,NULL,NULL,NULL,1644139988,1644139988),('Admin',1,NULL,NULL,NULL,1643693189,1643693189);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('Admin','/kategori/*'),('Admin','/informasi/*'),('Admin','/foto/*'),('Admin','/user/uploadPhoto'),('Admin','/video/*'),('Admin','/tentang/*'),('Admin','/site/index');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` VALUES ('route_rule',_binary 'O:30:\"mdm\\admin\\components\\RouteRule\":3:{s:4:\"name\";s:10:\"route_rule\";s:9:\"createdAt\";i:1579921277;s:9:\"updatedAt\";i:1579921277;}',1579921277,1579921277);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foto`
--

DROP TABLE IF EXISTS `foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idinformasi` int(11) NOT NULL,
  `nama_foto` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foto`
--

LOCK TABLES `foto` WRITE;
/*!40000 ALTER TABLE `foto` DISABLE KEYS */;
INSERT INTO `foto` VALUES (2,1,'../media/foto/61ff91221b6c2.jpeg');
/*!40000 ALTER TABLE `foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informasi`
--

DROP TABLE IF EXISTS `informasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkategori` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `lat` double NOT NULL,
  `long` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `informasi_idkategori_FK` (`idkategori`),
  CONSTRAINT `informasi_idkategori_FK` FOREIGN KEY (`idkategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informasi`
--

LOCK TABLES `informasi` WRITE;
/*!40000 ALTER TABLE `informasi` DISABLE KEYS */;
INSERT INTO `informasi` VALUES (1,1,'Kota Medan','tes',3.591737976714,98.680208389163);
/*!40000 ALTER TABLE `informasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (1,'Lokasi Wisata');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Kategori',NULL,'/kategori/index',NULL,'list-ul'),(2,'Informasi',NULL,'/informasi/index',NULL,'info'),(3,'Video',NULL,'/video/index',NULL,'youtube-play'),(4,'Tentang',NULL,'/tentang/index',NULL,'list-alt');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1570938090),('m140506_102106_rbac_init',1570938099),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1570938099),('m180523_151638_rbac_updates_indexes_without_prefix',1570938100);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tentang`
--

DROP TABLE IF EXISTS `tentang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tentang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tentang`
--

LOCK TABLES `tentang` WRITE;
/*!40000 ALTER TABLE `tentang` DISABLE KEYS */;
INSERT INTO `tentang` VALUES (1,'halo web site tanah karo');
/*!40000 ALTER TABLE `tentang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `password_reset_token` (`password_reset_token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'','admin','h0pAroLgO3MwuEN2X2ufdfXoeOfyzm77','$2y$13$uU6pldS6JjFvl4IMOisyDeTWKzmnv4WFGn3FgVjnBAD3sVCPVX18C',NULL,'admin@gmail.com',10,1561701675,1598928548,NULL,NULL),(13,'Rika Rosnelly','rika','mFLnP-l19FGZ_64zG6Dhqk3Em7umsfSu','$2y$13$MvA0ChGAfIaU5P9femzFN.wrh7TFj8.wxc9QmvxJkaQN9umg3a7g2',NULL,'rika@gmail.com',10,1570964681,1570965160,NULL,NULL),(23,'Linda','linda','tRubkf4aLk_AQCQvXP1zGxmbcJ6TW2wg','$2y$13$jwxnHc/PE70XkfsXYbTtQePXrq1FA/57/xSBW/Kq0NGQUjEt/tjPu',NULL,'linda@gmail.com',10,1573890891,1573890891,NULL,NULL),(27,'tes','tes','uKswxkV4Gk5tfZi6YDNxfRrvfoocs9lt','$2y$13$F86L5fKNMYJDhR8ezFcTs.bxIGRfQdI4ZhowcYg4aDPLYW3bsLiMS',NULL,'tes',10,1573978064,1573978064,NULL,NULL),(28,'yayasan','yayasan','vJtizAYl4Qw0giFfqWxXeNutYlpz0H8J','$2y$13$0sN.XXTHP4c5lOnDAkipHuFC5Gt66pNKmh8TWlyQD7FPVxlw8Z/2a',NULL,'yayasan@gamil.com',10,1598926148,1598926148,NULL,NULL),(29,'Admin Manajemen Informatika','prodi_manajemen_informatika','hwftwTJbamF1xCDF39K75eaNn3Bxdm1s','$2y$13$ZAGQ9elr9Uhpc.0.yDa8KeefsQeiWcbntx1qTAkCZYDUnAh5B6.vy',NULL,'prodi_mi@gmail.com',10,1602078713,1602080289,NULL,NULL),(30,'Admin Sistem Informasi','prodi_sistem_informasi','Ie4F0GyyaikK1ORbUxBQVLzQNwDY66lY','$2y$13$1j0uaulUYHZ4V0IsDK1ObOMPyrm71dInR3UaK5ut783Z2wp8dNeI6',NULL,'prodi_si@gmail.com',10,1602078761,1602080321,NULL,NULL),(31,'Admin Teknik Industri','prodi_teknik_industri','hc01BmREmsxB4GwUBHG4LQveYx56HkFy','$2y$13$IBBfiUCIUQacguFcNLP2iemzKDuGYfFJnD2.GipSmvU8.2GvTSFiu',NULL,'prodi_tind@gmail.com',10,1602078842,1602080348,NULL,NULL),(32,'Admin Teknik Informatika','prodi_teknik_informatika','WvJtYyoRU_I4mFu8OEsaxIGMPMldPm2i','$2y$13$A2X/sNyRf6Z3izGOP.JEWuT.DjVLkjIwKtYS62I0IK5dUx7OXMsde',NULL,'prodi_ti@gmail.com',10,1602078915,1602080371,NULL,NULL),(33,'Admin Psikologi','prodi_psikologi','ie7-u9bm9g7uv2dlm9dpcg5OFi-yIZ6S','$2y$13$8qtxM2f.GCZn9fgMZbKFwO04XLVoSqJcAQpU5Epz127pn/INLsFaq',NULL,'prodi_psi@gmail.com',10,1602078962,1602080399,NULL,NULL),(34,'Admin Akuntansi','prodi_akuntansi','oj4_8mrMdLPOgFT4gRx5p49cfECNCAwu','$2y$13$SlHdVDoTVAy3hiynk2DBOeStCmpkHZywemPKxaAvJMopEBWppEex6',NULL,'prodi_ak@gmail.com',10,1602079010,1602080449,NULL,NULL),(35,'Admin Ekonomi Syariah','prodi_ekonomi_syariah','ruY6Xc1v7e1SwQMgDjwJE39BiGUPqU1b','$2y$13$iuq87XrfHi/YOG7/49bH/.pG7tKZreusmvFya3jPlXcrt2jWTjAPO',NULL,'prodi_es@gmail.com',10,1602079083,1602080472,NULL,NULL),(36,'Admin Manajemen','prodi_manajemen','i8dmVdnDk0hwzy5Zew4j0QGoerHHrWqX','$2y$13$s2y75eLLnOhNHQTJgJjewODvs1ZWuKJe8qYLPmbG6LZ1e9S7VgpbW',NULL,'prodi_manajemen@gmail.com',10,1602079163,1602080494,NULL,NULL),(37,'Admin Perbankan Syariah','prodi_perbankan_syariah','LyjtuJ5Bs0zWqVb7uJxz67twFFggHKAM','$2y$13$AufEzQuNCECGG3bf97bec.A6OO.8Lj/pLqjbBe.m.U9VTZSEwdm2S',NULL,'prodi_perbankan_syariah@gmail.com',10,1602079236,1602080519,NULL,NULL),(38,'Admin Ilmu Hukum','prodi_ilmu_hukum','uwQT32xXl2pUt-wM93Rx7is36ToGA4Zp','$2y$13$ruiIL12PLf.tIojESpeEfeC1Q6L8fZbGIrmAuKjv9OY4YYIaMdk8m',NULL,'prodi_ilmu_hukum@gmail.com',10,1602079290,1602080539,NULL,NULL),(39,'Admin Ilmu Hubungan Internasional','prodi_hubungan_international','fytSXuFat_cE5QTAFHrrm_8ESVd8DyhE','$2y$13$bK1xnhRpshZsuxJlp/KGB.P62zTpAyZIB/WOfddvKGqX9nN/jqZyu',NULL,'prodi_hubungan_international@gmail.com',10,1602079349,1602080560,NULL,NULL),(40,'Admin Bahasa Inggris','prodi_bahasa_inggris','OsKO20iQ2zm15nLf53Zh1FwyAI7RZ85r','$2y$13$QG2zaGgm0gf4cfU0M790tOaiJHcD3EyiNcoxOCUbg/Z/nFN1rakRG',NULL,'prodi_bahasa_inggris@gmail.com',10,1602079391,1602080587,NULL,NULL),(41,'Admin Desain Interior','prodi_desain_interior','kkqRCtwAxqHi8qRRexekFMgUbX5Z0ZNb','$2y$13$Nj6fqJWiafAeoaHYSl.Wh.EUcDowEOXJQ8z8CIVr4tX/qY6vTIqpS',NULL,'prodi_desain_interior@gmail.com',10,1602079437,1602080607,NULL,NULL),(42,'Admin Desain Komunikasi Visual','prodi_desain_komunikasi_visual','JcVHXQ5MNNYv4QaRRHnbO144Oy9xjK7n','$2y$13$Y18xgoYquUGlz/ZuA6qevOmKixOsiDRBr1JbkCDLDasIivCorcj8q',NULL,'prodi_desain_komunikasi_visual@gmail.com',10,1602079490,1602080630,NULL,NULL),(43,'Admin Televisi Film','prodi_televisi_film','TOJ8e_qgxIn58-Iqp4XWGpdcckhWUXKV','$2y$13$aA6tp.K4xmWuB5zyK2z1OeXUXKjJx.HAFy8YZwWukzzaZkprhfTbm',NULL,'prodi_televisi_film@gmail.com',10,1602079535,1602080655,NULL,NULL),(44,'Admin Universitas','universitas','8woa4WuWRSvL_gJOxT0Um0zfHUF0HgtT','$2y$13$E74qXxcwoTGBTQSRu4fFcevVyUEVceBN3BJttrRxbcTqUJxSz1sBa',NULL,'universitas@gmail.com',10,1602838058,1602838428,NULL,NULL),(47,'PT. Suka Suka','sukasuka@gmail.com','UM_dNlH5ebVaef0rAxepsN4qRbWKCRi8','$2y$13$BTn9/F4131er56S7bip21.j7cq6M9qptRFmSYJk9PfVlHMlg6jNQO',NULL,'sukasuka@gmail.com',10,1640097834,1642431002,NULL,'Kategori');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `url_image` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'gis_akpar'
--

--
-- Dumping routines for database 'gis_akpar'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-01 21:31:52
