/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100419
 Source Host           : localhost:3306
 Source Schema         : db_akuntansi

 Target Server Type    : MySQL
 Target Server Version : 100419
 File Encoding         : 65001

 Date: 17/01/2022 21:52:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for akun
-- ----------------------------
DROP TABLE IF EXISTS `akun`;
CREATE TABLE `akun`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_akun` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nilai` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of akun
-- ----------------------------
INSERT INTO `akun` VALUES (1, 'Harta', 5);
INSERT INTO `akun` VALUES (2, 'Hutang', 1);
INSERT INTO `akun` VALUES (3, 'Modal', 2);
INSERT INTO `akun` VALUES (4, 'Pendapatan', 3);
INSERT INTO `akun` VALUES (5, 'Beban', 28);
INSERT INTO `akun` VALUES (6, 'Piutang', 1);

-- ----------------------------
-- Table structure for anggaran
-- ----------------------------
DROP TABLE IF EXISTS `anggaran`;
CREATE TABLE `anggaran`  (
  `id` int(11) NOT NULL,
  `idprodi` int(255) NOT NULL,
  `kegiatan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `idtahunajaran` int(11) NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Menunggu',
  `tanggal` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of anggaran
-- ----------------------------
INSERT INTO `anggaran` VALUES (1, 1, 'etwet', 8889990, 1, 'Disetujui', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (2, 1, 'afasdf', 99999, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (4, 1, 'sdgasd', 235235, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (5, 1, 'twet', 534534, 2, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (6, 1, 'Hubungan Masyarakat', 9000000, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (7, 1, 'Hubungan Masyarakat1', 900400, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (8, 1, 'Hubungan Masyarakat2', 244444, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (9, 1, 'Hubungan Masyarakat3', 5666, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (10, 1, 'Hubungan Masyarakat4', 11111, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (11, 2, 'Seminar Nasional', 5000000, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (12, 2, 'Workshop', 3000000, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (14, 2, 'Pelatihan', 1500000, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (15, 2, 'Hubungan Masyarakat', 9000000, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (16, 2, 'Hubungan Masyarakat1', 900400, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (17, 2, 'Hubungan Masyarakat2', 244444, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (18, 2, 'Hubungan Masyarakat3', 5666, 1, 'Menunggu', '2021-12-21 09:29:11');
INSERT INTO `anggaran` VALUES (19, 2, 'Hubungan Masyarakat4', 11111, 1, 'Menunggu', '2021-12-21 09:29:11');

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment`  (
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
INSERT INTO `auth_assignment` VALUES ('Admin Aplikasi', '1', 1570941067);
INSERT INTO `auth_assignment` VALUES ('Admin Aplikasi', '27', 1573978064);
INSERT INTO `auth_assignment` VALUES ('Admin Fakultas', '23', 1573890891);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '22', 1573884574);
INSERT INTO `auth_assignment` VALUES ('Rektor', '13', 1573889402);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '29', 1602078713);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '30', 1602078761);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '31', 1602078842);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '32', 1602078915);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '33', 1602078962);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '34', 1602079010);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '35', 1602079083);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '36', 1602079163);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '37', 1602079236);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '38', 1602079290);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '39', 1602079349);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '40', 1602079391);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '41', 1602079437);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '42', 1602079490);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '43', 1602079535);
INSERT INTO `auth_assignment` VALUES ('Admin Program Studi', '44', 1602838058);
INSERT INTO `auth_assignment` VALUES ('Merchant', '46', 1640097736);
INSERT INTO `auth_assignment` VALUES ('Merchant', '47', 1640097834);

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `data` blob NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  INDEX `rule_name`(`rule_name`) USING BTREE,
  INDEX `idx-auth_item-type`(`type`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('/admin/user/logout', 2, NULL, NULL, NULL, 1640697720, 1640697720);
INSERT INTO `auth_item` VALUES ('/akun/bulk-delete', 2, NULL, NULL, NULL, 1581909004, 1581909004);
INSERT INTO `auth_item` VALUES ('/akun/create', 2, NULL, NULL, NULL, 1581909004, 1581909004);
INSERT INTO `auth_item` VALUES ('/akun/delete', 2, NULL, NULL, NULL, 1581909004, 1581909004);
INSERT INTO `auth_item` VALUES ('/akun/index', 2, NULL, NULL, NULL, 1581909003, 1581909003);
INSERT INTO `auth_item` VALUES ('/akun/update', 2, NULL, NULL, NULL, 1581909004, 1581909004);
INSERT INTO `auth_item` VALUES ('/akun/view', 2, NULL, NULL, NULL, 1581909004, 1581909004);
INSERT INTO `auth_item` VALUES ('/anggaran/bulk-delete', 2, NULL, NULL, NULL, 1572143740, 1572143740);
INSERT INTO `auth_item` VALUES ('/anggaran/cetaklaporan', 2, NULL, NULL, NULL, 1573269051, 1573269051);
INSERT INTO `auth_item` VALUES ('/anggaran/create', 2, NULL, NULL, NULL, 1572143740, 1572143740);
INSERT INTO `auth_item` VALUES ('/anggaran/delete', 2, NULL, NULL, NULL, 1572143740, 1572143740);
INSERT INTO `auth_item` VALUES ('/anggaran/import', 2, NULL, NULL, NULL, 1572676037, 1572676037);
INSERT INTO `auth_item` VALUES ('/anggaran/index', 2, NULL, NULL, NULL, 1572143740, 1572143740);
INSERT INTO `auth_item` VALUES ('/anggaran/laporan', 2, NULL, NULL, NULL, 1573269051, 1573269051);
INSERT INTO `auth_item` VALUES ('/anggaran/pilihprodi', 2, NULL, NULL, NULL, 1573274648, 1573274648);
INSERT INTO `auth_item` VALUES ('/anggaran/update', 2, NULL, NULL, NULL, 1572143740, 1572143740);
INSERT INTO `auth_item` VALUES ('/anggaran/view', 2, NULL, NULL, NULL, 1572143740, 1572143740);
INSERT INTO `auth_item` VALUES ('/bukubesar/index', 2, NULL, NULL, NULL, 1583045780, 1583045780);
INSERT INTO `auth_item` VALUES ('/fakultas/bulk-delete', 2, NULL, NULL, NULL, 1570940878, 1570940878);
INSERT INTO `auth_item` VALUES ('/fakultas/create', 2, NULL, NULL, NULL, 1570940878, 1570940878);
INSERT INTO `auth_item` VALUES ('/fakultas/delete', 2, NULL, NULL, NULL, 1570940878, 1570940878);
INSERT INTO `auth_item` VALUES ('/fakultas/index', 2, NULL, NULL, NULL, 1570940878, 1570940878);
INSERT INTO `auth_item` VALUES ('/fakultas/update', 2, NULL, NULL, NULL, 1570940878, 1570940878);
INSERT INTO `auth_item` VALUES ('/fakultas/view', 2, NULL, NULL, NULL, 1570940878, 1570940878);
INSERT INTO `auth_item` VALUES ('/gridview/export/download', 2, NULL, NULL, NULL, 1572688477, 1572688477);
INSERT INTO `auth_item` VALUES ('/hutang/*', 2, NULL, NULL, NULL, 1641307374, 1641307374);
INSERT INTO `auth_item` VALUES ('/hutang/index', 2, NULL, NULL, NULL, 1641307374, 1641307374);
INSERT INTO `auth_item` VALUES ('/jenispendapatan/bulk-delete', 2, NULL, NULL, NULL, 1571061689, 1571061689);
INSERT INTO `auth_item` VALUES ('/jenispendapatan/create', 2, NULL, NULL, NULL, 1571061689, 1571061689);
INSERT INTO `auth_item` VALUES ('/jenispendapatan/delete', 2, NULL, NULL, NULL, 1571061689, 1571061689);
INSERT INTO `auth_item` VALUES ('/jenispendapatan/index', 2, NULL, NULL, NULL, 1571061689, 1571061689);
INSERT INTO `auth_item` VALUES ('/jenispendapatan/update', 2, NULL, NULL, NULL, 1571061689, 1571061689);
INSERT INTO `auth_item` VALUES ('/jenispendapatan/view', 2, NULL, NULL, NULL, 1571061689, 1571061689);
INSERT INTO `auth_item` VALUES ('/jenispengeluaran/bulk-delete', 2, NULL, NULL, NULL, 1571539479, 1571539479);
INSERT INTO `auth_item` VALUES ('/jenispengeluaran/create', 2, NULL, NULL, NULL, 1571539479, 1571539479);
INSERT INTO `auth_item` VALUES ('/jenispengeluaran/delete', 2, NULL, NULL, NULL, 1571539479, 1571539479);
INSERT INTO `auth_item` VALUES ('/jenispengeluaran/index', 2, NULL, NULL, NULL, 1571539478, 1571539478);
INSERT INTO `auth_item` VALUES ('/jenispengeluaran/update', 2, NULL, NULL, NULL, 1571539479, 1571539479);
INSERT INTO `auth_item` VALUES ('/jenispengeluaran/view', 2, NULL, NULL, NULL, 1571539479, 1571539479);
INSERT INTO `auth_item` VALUES ('/jumlah/bulk-delete', 2, NULL, NULL, NULL, 1570953266, 1570953266);
INSERT INTO `auth_item` VALUES ('/jumlah/create', 2, NULL, NULL, NULL, 1570953265, 1570953265);
INSERT INTO `auth_item` VALUES ('/jumlah/delete', 2, NULL, NULL, NULL, 1570953266, 1570953266);
INSERT INTO `auth_item` VALUES ('/jumlah/index', 2, NULL, NULL, NULL, 1570953265, 1570953265);
INSERT INTO `auth_item` VALUES ('/jumlah/update', 2, NULL, NULL, NULL, 1570953266, 1570953266);
INSERT INTO `auth_item` VALUES ('/jumlah/view', 2, NULL, NULL, NULL, 1570953265, 1570953265);
INSERT INTO `auth_item` VALUES ('/jurnalumum/index', 2, NULL, NULL, NULL, 1583545248, 1583545248);
INSERT INTO `auth_item` VALUES ('/kategoriakun/*', 2, NULL, NULL, NULL, 1598862350, 1598862350);
INSERT INTO `auth_item` VALUES ('/kategoriakun/bulk-delete', 2, NULL, NULL, NULL, 1598862349, 1598862349);
INSERT INTO `auth_item` VALUES ('/kategoriakun/create', 2, NULL, NULL, NULL, 1598862349, 1598862349);
INSERT INTO `auth_item` VALUES ('/kategoriakun/delete', 2, NULL, NULL, NULL, 1598862349, 1598862349);
INSERT INTO `auth_item` VALUES ('/kategoriakun/index', 2, NULL, NULL, NULL, 1598862349, 1598862349);
INSERT INTO `auth_item` VALUES ('/kategoriakun/update', 2, NULL, NULL, NULL, 1598862349, 1598862349);
INSERT INTO `auth_item` VALUES ('/kategoriakun/view', 2, NULL, NULL, NULL, 1598862349, 1598862349);
INSERT INTO `auth_item` VALUES ('/labarugi/index', 2, NULL, NULL, NULL, 1583994530, 1583994530);
INSERT INTO `auth_item` VALUES ('/merchant/*', 2, NULL, NULL, NULL, 1640095940, 1640095940);
INSERT INTO `auth_item` VALUES ('/merchant/bulk-delete', 2, NULL, NULL, NULL, 1640095940, 1640095940);
INSERT INTO `auth_item` VALUES ('/merchant/create', 2, NULL, NULL, NULL, 1640095939, 1640095939);
INSERT INTO `auth_item` VALUES ('/merchant/delete', 2, NULL, NULL, NULL, 1640095940, 1640095940);
INSERT INTO `auth_item` VALUES ('/merchant/index', 2, NULL, NULL, NULL, 1640095939, 1640095939);
INSERT INTO `auth_item` VALUES ('/merchant/update', 2, NULL, NULL, NULL, 1640095940, 1640095940);
INSERT INTO `auth_item` VALUES ('/merchant/view', 2, NULL, NULL, NULL, 1640095939, 1640095939);
INSERT INTO `auth_item` VALUES ('/neraca/*', 2, NULL, NULL, NULL, 1598868511, 1598868511);
INSERT INTO `auth_item` VALUES ('/neraca/index', 2, NULL, NULL, NULL, 1598868511, 1598868511);
INSERT INTO `auth_item` VALUES ('/neracasaldo/index', 2, NULL, NULL, NULL, 1583543865, 1583543865);
INSERT INTO `auth_item` VALUES ('/pejabat/bulk-delete', 2, NULL, NULL, NULL, 1570955590, 1570955590);
INSERT INTO `auth_item` VALUES ('/pejabat/create', 2, NULL, NULL, NULL, 1570955589, 1570955589);
INSERT INTO `auth_item` VALUES ('/pejabat/delete', 2, NULL, NULL, NULL, 1570955590, 1570955590);
INSERT INTO `auth_item` VALUES ('/pejabat/index', 2, NULL, NULL, NULL, 1570955589, 1570955589);
INSERT INTO `auth_item` VALUES ('/pejabat/update', 2, NULL, NULL, NULL, 1570955589, 1570955589);
INSERT INTO `auth_item` VALUES ('/pejabat/view', 2, NULL, NULL, NULL, 1570955589, 1570955589);
INSERT INTO `auth_item` VALUES ('/pendapatan/bulk-delete', 2, NULL, NULL, NULL, 1571062475, 1571062475);
INSERT INTO `auth_item` VALUES ('/pendapatan/cetaklaporan', 2, NULL, NULL, NULL, 1572760612, 1572760612);
INSERT INTO `auth_item` VALUES ('/pendapatan/create', 2, NULL, NULL, NULL, 1571062475, 1571062475);
INSERT INTO `auth_item` VALUES ('/pendapatan/delete', 2, NULL, NULL, NULL, 1571062475, 1571062475);
INSERT INTO `auth_item` VALUES ('/pendapatan/import', 2, NULL, NULL, NULL, 1572676533, 1572676533);
INSERT INTO `auth_item` VALUES ('/pendapatan/index', 2, NULL, NULL, NULL, 1571062475, 1571062475);
INSERT INTO `auth_item` VALUES ('/pendapatan/laporan', 2, NULL, NULL, NULL, 1571538930, 1571538930);
INSERT INTO `auth_item` VALUES ('/pendapatan/update', 2, NULL, NULL, NULL, 1571062475, 1571062475);
INSERT INTO `auth_item` VALUES ('/pendapatan/view', 2, NULL, NULL, NULL, 1571062475, 1571062475);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/bulk-delete', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/cetaklaporan', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/create', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/delete', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/import', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/index', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/laporan', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/update', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pendapatanjasa/view', 2, NULL, NULL, NULL, 1579925712, 1579925712);
INSERT INTO `auth_item` VALUES ('/pengeluaran/bulk-delete', 2, NULL, NULL, NULL, 1571539730, 1571539730);
INSERT INTO `auth_item` VALUES ('/pengeluaran/cetaklaporan', 2, NULL, NULL, NULL, 1573283315, 1573283315);
INSERT INTO `auth_item` VALUES ('/pengeluaran/cetaklaporanpengeluaran', 2, NULL, NULL, NULL, 1582006413, 1582006413);
INSERT INTO `auth_item` VALUES ('/pengeluaran/create', 2, NULL, NULL, NULL, 1571539730, 1571539730);
INSERT INTO `auth_item` VALUES ('/pengeluaran/delete', 2, NULL, NULL, NULL, 1571539730, 1571539730);
INSERT INTO `auth_item` VALUES ('/pengeluaran/import', 2, NULL, NULL, NULL, 1572670954, 1572670954);
INSERT INTO `auth_item` VALUES ('/pengeluaran/index', 2, NULL, NULL, NULL, 1571539730, 1571539730);
INSERT INTO `auth_item` VALUES ('/pengeluaran/laporan', 2, NULL, NULL, NULL, 1573283315, 1573283315);
INSERT INTO `auth_item` VALUES ('/pengeluaran/laporanlpj', 2, NULL, NULL, NULL, 1582004137, 1582004137);
INSERT INTO `auth_item` VALUES ('/pengeluaran/laporanpengaluaran', 2, NULL, NULL, NULL, 1581338159, 1581338159);
INSERT INTO `auth_item` VALUES ('/pengeluaran/pilihprodi', 2, NULL, NULL, NULL, 1573283466, 1573283466);
INSERT INTO `auth_item` VALUES ('/pengeluaran/update', 2, NULL, NULL, NULL, 1571539730, 1571539730);
INSERT INTO `auth_item` VALUES ('/pengeluaran/view', 2, NULL, NULL, NULL, 1571539730, 1571539730);
INSERT INTO `auth_item` VALUES ('/piutang/*', 2, NULL, NULL, NULL, 1641306411, 1641306411);
INSERT INTO `auth_item` VALUES ('/piutang/index', 2, NULL, NULL, NULL, 1641306411, 1641306411);
INSERT INTO `auth_item` VALUES ('/prodi/bulk-delete', 2, NULL, NULL, NULL, 1570943273, 1570943273);
INSERT INTO `auth_item` VALUES ('/prodi/create', 2, NULL, NULL, NULL, 1570943273, 1570943273);
INSERT INTO `auth_item` VALUES ('/prodi/delete', 2, NULL, NULL, NULL, 1570943273, 1570943273);
INSERT INTO `auth_item` VALUES ('/prodi/index', 2, NULL, NULL, NULL, 1570943273, 1570943273);
INSERT INTO `auth_item` VALUES ('/prodi/update', 2, NULL, NULL, NULL, 1570943273, 1570943273);
INSERT INTO `auth_item` VALUES ('/prodi/view', 2, NULL, NULL, NULL, 1570943273, 1570943273);
INSERT INTO `auth_item` VALUES ('/saldo-awal/*', 2, NULL, NULL, NULL, 1587452159, 1587452159);
INSERT INTO `auth_item` VALUES ('/saldo-awal/bulk-delete', 2, NULL, NULL, NULL, 1587452159, 1587452159);
INSERT INTO `auth_item` VALUES ('/saldo-awal/create', 2, NULL, NULL, NULL, 1587452159, 1587452159);
INSERT INTO `auth_item` VALUES ('/saldo-awal/delete', 2, NULL, NULL, NULL, 1587452159, 1587452159);
INSERT INTO `auth_item` VALUES ('/saldo-awal/index', 2, NULL, NULL, NULL, 1587452158, 1587452158);
INSERT INTO `auth_item` VALUES ('/saldo-awal/update', 2, NULL, NULL, NULL, 1587452159, 1587452159);
INSERT INTO `auth_item` VALUES ('/saldo-awal/view', 2, NULL, NULL, NULL, 1587452159, 1587452159);
INSERT INTO `auth_item` VALUES ('/settahunajaran/bulk-delete', 2, NULL, NULL, NULL, 1572190889, 1572190889);
INSERT INTO `auth_item` VALUES ('/settahunajaran/create', 2, NULL, NULL, NULL, 1572190889, 1572190889);
INSERT INTO `auth_item` VALUES ('/settahunajaran/delete', 2, NULL, NULL, NULL, 1572190889, 1572190889);
INSERT INTO `auth_item` VALUES ('/settahunajaran/index', 2, NULL, NULL, NULL, 1572190889, 1572190889);
INSERT INTO `auth_item` VALUES ('/settahunajaran/update', 2, NULL, NULL, NULL, 1572190889, 1572190889);
INSERT INTO `auth_item` VALUES ('/settahunajaran/view', 2, NULL, NULL, NULL, 1572190889, 1572190889);
INSERT INTO `auth_item` VALUES ('/site/logout', 2, NULL, NULL, NULL, 1640697720, 1640697720);
INSERT INTO `auth_item` VALUES ('/subakun/bulk-delete', 2, NULL, NULL, NULL, 1581909501, 1581909501);
INSERT INTO `auth_item` VALUES ('/subakun/create', 2, NULL, NULL, NULL, 1581909501, 1581909501);
INSERT INTO `auth_item` VALUES ('/subakun/delete', 2, NULL, NULL, NULL, 1581909501, 1581909501);
INSERT INTO `auth_item` VALUES ('/subakun/index', 2, NULL, NULL, NULL, 1581909501, 1581909501);
INSERT INTO `auth_item` VALUES ('/subakun/pilihkategoriakun', 2, NULL, NULL, NULL, 1598867842, 1598867842);
INSERT INTO `auth_item` VALUES ('/subakun/update', 2, NULL, NULL, NULL, 1581909501, 1581909501);
INSERT INTO `auth_item` VALUES ('/subakun/view', 2, NULL, NULL, NULL, 1581909501, 1581909501);
INSERT INTO `auth_item` VALUES ('/tahunajaran/bulk-delete', 2, NULL, NULL, NULL, 1570954173, 1570954173);
INSERT INTO `auth_item` VALUES ('/tahunajaran/create', 2, NULL, NULL, NULL, 1570954173, 1570954173);
INSERT INTO `auth_item` VALUES ('/tahunajaran/delete', 2, NULL, NULL, NULL, 1570954173, 1570954173);
INSERT INTO `auth_item` VALUES ('/tahunajaran/index', 2, NULL, NULL, NULL, 1570954173, 1570954173);
INSERT INTO `auth_item` VALUES ('/tahunajaran/update', 2, NULL, NULL, NULL, 1570954173, 1570954173);
INSERT INTO `auth_item` VALUES ('/tahunajaran/view', 2, NULL, NULL, NULL, 1570954173, 1570954173);
INSERT INTO `auth_item` VALUES ('/transaksi/bulk-delete', 2, NULL, NULL, NULL, 1582425210, 1582425210);
INSERT INTO `auth_item` VALUES ('/transaksi/create', 2, NULL, NULL, NULL, 1582425210, 1582425210);
INSERT INTO `auth_item` VALUES ('/transaksi/delete', 2, NULL, NULL, NULL, 1582425210, 1582425210);
INSERT INTO `auth_item` VALUES ('/transaksi/index', 2, NULL, NULL, NULL, 1582425210, 1582425210);
INSERT INTO `auth_item` VALUES ('/transaksi/pilihsubakun', 2, NULL, NULL, NULL, 1582426889, 1582426889);
INSERT INTO `auth_item` VALUES ('/transaksi/update', 2, NULL, NULL, NULL, 1582425210, 1582425210);
INSERT INTO `auth_item` VALUES ('/transaksi/view', 2, NULL, NULL, NULL, 1582425210, 1582425210);
INSERT INTO `auth_item` VALUES ('/user/change-password', 2, NULL, NULL, NULL, 1598928221, 1598928221);
INSERT INTO `auth_item` VALUES ('/userpejabat/bulk-delete', 2, NULL, NULL, NULL, 1570958434, 1570958434);
INSERT INTO `auth_item` VALUES ('/userpejabat/create', 2, NULL, NULL, NULL, 1570958433, 1570958433);
INSERT INTO `auth_item` VALUES ('/userpejabat/delete', 2, NULL, NULL, NULL, 1570958433, 1570958433);
INSERT INTO `auth_item` VALUES ('/userpejabat/index', 2, NULL, NULL, NULL, 1570958433, 1570958433);
INSERT INTO `auth_item` VALUES ('/userpejabat/update', 2, NULL, NULL, NULL, 1570958433, 1570958433);
INSERT INTO `auth_item` VALUES ('/userpejabat/view', 2, NULL, NULL, NULL, 1570958433, 1570958433);
INSERT INTO `auth_item` VALUES ('Admin Aplikasi', 1, NULL, NULL, NULL, 1570941014, 1573884947);
INSERT INTO `auth_item` VALUES ('Administrasi', 1, NULL, NULL, NULL, 1640096865, 1640096865);
INSERT INTO `auth_item` VALUES ('Merchant', 1, NULL, NULL, NULL, 1640097078, 1640097078);

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child`  (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/akun/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/akun/create');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/akun/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/akun/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/akun/update');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/akun/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/create');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/create');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/create');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/create');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/delete');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/import');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/import');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/import');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/import');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/import');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/index');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/index');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/index');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/index');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/anggaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/anggaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/update');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/update');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/update');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/update');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/anggaran/view');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/anggaran/view');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/anggaran/view');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/anggaran/view');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/anggaran/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/bukubesar/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/fakultas/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/fakultas/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/fakultas/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/fakultas/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/fakultas/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/fakultas/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/gridview/export/download');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/gridview/export/download');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/gridview/export/download');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/gridview/export/download');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/gridview/export/download');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/gridview/export/download');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispendapatan/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispendapatan/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispendapatan/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispendapatan/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispendapatan/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispendapatan/view');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispengeluaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispengeluaran/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispengeluaran/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispengeluaran/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispengeluaran/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/jenispengeluaran/view');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/jumlah/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/jumlah/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/jumlah/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/jumlah/create');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/jumlah/create');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/jumlah/create');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/jumlah/delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/jumlah/delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/jumlah/delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/jumlah/index');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/jumlah/index');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/jumlah/index');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/jumlah/update');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/jumlah/update');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/jumlah/update');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/jumlah/view');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/jumlah/view');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/jumlah/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/jurnalumum/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/labarugi/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/neracasaldo/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/pejabat/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/pejabat/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/pejabat/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/pejabat/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/pejabat/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/pejabat/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/create');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/create');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/create');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/create');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/import');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/import');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/import');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/import');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/index');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/index');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/index');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/laporan');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/laporan');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/laporan');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/laporan');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/update');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/update');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/update');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/update');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatan/view');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pendapatan/view');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pendapatan/view');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pendapatan/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/create');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/import');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/laporan');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/update');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pendapatanjasa/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/cetaklaporanpengeluaran');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/create');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/create');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/create');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/create');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/create');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/delete');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/delete');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/delete');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/delete');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/import');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/import');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/import');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/import');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/import');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/import');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/index');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/index');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/index');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/index');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/laporan');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/laporanlpj');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/laporanpengaluaran');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/pilihprodi');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/update');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/update');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/update');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/update');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/update');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/pengeluaran/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pengeluaran/view');
INSERT INTO `auth_item_child` VALUES ('Dekan', '/pengeluaran/view');
INSERT INTO `auth_item_child` VALUES ('Kaprodi', '/pengeluaran/view');
INSERT INTO `auth_item_child` VALUES ('Rektor', '/pengeluaran/view');
INSERT INTO `auth_item_child` VALUES ('Yayasan', '/pengeluaran/view');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/prodi/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/prodi/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/prodi/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/prodi/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/prodi/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/prodi/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/saldo-awal/*');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/saldo-awal/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/saldo-awal/create');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/saldo-awal/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/saldo-awal/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/saldo-awal/update');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/saldo-awal/view');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/settahunajaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/settahunajaran/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/settahunajaran/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/settahunajaran/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/settahunajaran/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/settahunajaran/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/subakun/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/subakun/create');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/subakun/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/subakun/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/subakun/update');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/subakun/view');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/tahunajaran/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/tahunajaran/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/tahunajaran/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/tahunajaran/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/tahunajaran/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/tahunajaran/view');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/transaksi/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/transaksi/create');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/transaksi/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/transaksi/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/transaksi/pilihsubakun');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/transaksi/update');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/transaksi/view');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/userpejabat/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/userpejabat/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/userpejabat/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/userpejabat/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/userpejabat/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/userpejabat/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/cetaklaporan');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/import');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/laporan');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/pendapatan/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/akun/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/akun/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/akun/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/akun/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/akun/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/akun/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/labarugi/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/neraca/*');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/neraca/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/neracasaldo/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/saldo-awal/*');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/saldo-awal/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/saldo-awal/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/saldo-awal/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/saldo-awal/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/saldo-awal/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/saldo-awal/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/subakun/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/subakun/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/subakun/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/subakun/pilihkategoriakun');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/subakun/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/subakun/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/transaksi/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/transaksi/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/transaksi/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/transaksi/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/transaksi/pilihsubakun');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/transaksi/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/transaksi/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/subakun/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/kategoriakun/*');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/kategoriakun/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/kategoriakun/create');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/kategoriakun/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/kategoriakun/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/kategoriakun/update');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/kategoriakun/view');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/jurnalumum/index');
INSERT INTO `auth_item_child` VALUES ('Admin Program Studi', '/bukubesar/index');
INSERT INTO `auth_item_child` VALUES ('Admin Fakultas', '/neraca/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/merchant/*');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/merchant/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/merchant/create');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/merchant/delete');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/merchant/index');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/merchant/update');
INSERT INTO `auth_item_child` VALUES ('Admin Aplikasi', '/merchant/view');
INSERT INTO `auth_item_child` VALUES ('Administrasi', '/merchant/*');
INSERT INTO `auth_item_child` VALUES ('Administrasi', '/site/logout');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/subakun/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/subakun/create');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/subakun/delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/subakun/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/subakun/pilihkategoriakun');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/subakun/update');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/subakun/view');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/transaksi/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/transaksi/create');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/transaksi/delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/transaksi/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/transaksi/pilihsubakun');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/transaksi/update');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/transaksi/view');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/hutang/*');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/hutang/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/hutang/create');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/hutang/delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/hutang/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/hutang/update');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/hutang/view');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/piutang/*');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/piutang/bulk-delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/piutang/create');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/piutang/delete');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/piutang/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/piutang/update');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/piutang/view');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/bukubesar/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/jurnalumum/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/labarugi/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/neraca/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/neracasaldo/index');
INSERT INTO `auth_item_child` VALUES ('Merchant', '/user/change-password');

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` blob NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_rule
-- ----------------------------
INSERT INTO `auth_rule` VALUES ('route_rule', 0x4F3A33303A226D646D5C61646D696E5C636F6D706F6E656E74735C526F75746552756C65223A333A7B733A343A226E616D65223B733A31303A22726F7574655F72756C65223B733A393A22637265617465644174223B693A313537393932313237373B733A393A22757064617465644174223B693A313537393932313237373B7D, 1579921277, 1579921277);

-- ----------------------------
-- Table structure for fakultas
-- ----------------------------
DROP TABLE IF EXISTS `fakultas`;
CREATE TABLE `fakultas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_fakultas` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fakultas
-- ----------------------------
INSERT INTO `fakultas` VALUES (1, 'Teknik dan Ilmu Komputer');
INSERT INTO `fakultas` VALUES (2, 'Psikologi');
INSERT INTO `fakultas` VALUES (4, 'Ekonomi dan Bisnis');
INSERT INTO `fakultas` VALUES (5, 'Hukum');
INSERT INTO `fakultas` VALUES (6, 'Ilmu Politik dan Kependidikan');
INSERT INTO `fakultas` VALUES (7, 'Seni dan Desain');
INSERT INTO `fakultas` VALUES (8, 'Universitas Potensi Utama');

-- ----------------------------
-- Table structure for hutang
-- ----------------------------
DROP TABLE IF EXISTS `hutang`;
CREATE TABLE `hutang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsubakun` int(11) NOT NULL,
  `total` bigint(20) NOT NULL,
  `keterangan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tanggal` date NULL DEFAULT NULL,
  `tanggal_bayar` timestamp(0) NULL DEFAULT NULL,
  `tanggal_insert` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_hutang_idsubakun`(`idsubakun`) USING BTREE,
  CONSTRAINT `fk_hutang_idsubakun` FOREIGN KEY (`idsubakun`) REFERENCES `sub_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hutang
-- ----------------------------
INSERT INTO `hutang` VALUES (2, 219, 200000, '-', '2021-12-30', NULL, '2021-12-30 21:19:14');
INSERT INTO `hutang` VALUES (3, 219, 1000000, 'Hutang ke PT. Jaya Kusuma', '2022-01-04', NULL, '2022-01-04 21:44:14');

-- ----------------------------
-- Table structure for jenis_pendapatan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_pendapatan`;
CREATE TABLE `jenis_pendapatan`  (
  `id` int(11) NOT NULL,
  `jenis_pendapatan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kategori` varchar(50) CHARACTER SET macce COLLATE macce_general_ci NOT NULL DEFAULT ''
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jenis_pendapatan
-- ----------------------------
INSERT INTO `jenis_pendapatan` VALUES (1, 'Uang Kuliah', 'Mahasiswa');
INSERT INTO `jenis_pendapatan` VALUES (2, 'Uang Ujian', 'Mahasiswa');
INSERT INTO `jenis_pendapatan` VALUES (3, 'Penelitian', 'Dosen');
INSERT INTO `jenis_pendapatan` VALUES (6, 'Lain lain', 'Lain-lain');

-- ----------------------------
-- Table structure for jenis_pengeluaran
-- ----------------------------
DROP TABLE IF EXISTS `jenis_pengeluaran`;
CREATE TABLE `jenis_pengeluaran`  (
  `id` int(11) NOT NULL,
  `jenis_pengeluaran` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jenis_pengeluaran
-- ----------------------------
INSERT INTO `jenis_pengeluaran` VALUES (1, 'Seminar');
INSERT INTO `jenis_pengeluaran` VALUES (2, 'Pengabdian Masyarakat');
INSERT INTO `jenis_pengeluaran` VALUES (3, 'Sarana dan Prasarana');

-- ----------------------------
-- Table structure for jumlah
-- ----------------------------
DROP TABLE IF EXISTS `jumlah`;
CREATE TABLE `jumlah`  (
  `id` int(11) NOT NULL,
  `idprodi` int(11) NOT NULL,
  `idtahunajaran` int(11) NOT NULL,
  `jumlah_mahasiswa` int(11) NOT NULL,
  `jumlah_dosen` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jumlah
-- ----------------------------
INSERT INTO `jumlah` VALUES (1, 1, 1, 700, 150);
INSERT INTO `jumlah` VALUES (2, 2, 1, 400, 507);

-- ----------------------------
-- Table structure for kategori_akun
-- ----------------------------
DROP TABLE IF EXISTS `kategori_akun`;
CREATE TABLE `kategori_akun`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idakun` int(11) NOT NULL,
  `nama_kategori` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_kategori_akun_idakun`(`idakun`) USING BTREE,
  CONSTRAINT `fk_kategori_akun_idakun` FOREIGN KEY (`idakun`) REFERENCES `akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kategori_akun
-- ----------------------------
INSERT INTO `kategori_akun` VALUES (1, 1, 'Harta Lancar');
INSERT INTO `kategori_akun` VALUES (2, 1, 'Harta Tetap');
INSERT INTO `kategori_akun` VALUES (3, 2, 'Utang Lancar');
INSERT INTO `kategori_akun` VALUES (4, 2, 'Utang Jangka Panjang');
INSERT INTO `kategori_akun` VALUES (5, 4, 'Pendapatan Operasional');
INSERT INTO `kategori_akun` VALUES (6, 5, 'Beban Lain-lain');
INSERT INTO `kategori_akun` VALUES (7, 5, 'Beban Operasional');
INSERT INTO `kategori_akun` VALUES (8, 5, 'Beban Penelitian');
INSERT INTO `kategori_akun` VALUES (9, 5, 'Beban PkM');
INSERT INTO `kategori_akun` VALUES (10, 5, 'Beban Investasi Sarana');
INSERT INTO `kategori_akun` VALUES (11, 5, 'Beban Investasi Prasarana');
INSERT INTO `kategori_akun` VALUES (12, 5, 'Beban Investasi SDM');

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `id` int(11) NOT NULL,
  `nim` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idprodi` int(11) NULL DEFAULT NULL,
  `angkatan` int(5) NULL DEFAULT NULL,
  `semester` int(2) NULL DEFAULT NULL,
  `tagihan` bigint(20) NULL DEFAULT NULL,
  `biaya_pendidikan` bigint(20) NULL DEFAULT NULL,
  `total_bayar` bigint(20) NULL DEFAULT NULL,
  `tanggal_bayar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent` int(11) NULL DEFAULT NULL,
  `route` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order` int(11) NULL DEFAULT NULL,
  `data` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'Fakultas', NULL, '/fakultas/index', NULL, NULL);
INSERT INTO `menu` VALUES (2, 'Program Studi', NULL, '/prodi/index', NULL, NULL);
INSERT INTO `menu` VALUES (3, 'Jumlah', NULL, '/jumlah/index', NULL, NULL);
INSERT INTO `menu` VALUES (4, 'Tahun Ajaran', NULL, '/tahunajaran/index', NULL, NULL);
INSERT INTO `menu` VALUES (5, 'Pejabat', NULL, '/pejabat/index', NULL, NULL);
INSERT INTO `menu` VALUES (6, 'User Pejabat', NULL, '/userpejabat/index', NULL, NULL);
INSERT INTO `menu` VALUES (7, 'Jenis Pendapatan', NULL, '/jenispendapatan/index', NULL, NULL);
INSERT INTO `menu` VALUES (8, 'Pendapatan', NULL, '/pendapatan/index', NULL, 'cloud-download');
INSERT INTO `menu` VALUES (11, 'Jenis Pengeluaran', NULL, '/jenispengeluaran/index', NULL, NULL);
INSERT INTO `menu` VALUES (12, 'Laporan', NULL, NULL, NULL, 'newspaper-o');
INSERT INTO `menu` VALUES (14, 'Pengguna Dana', NULL, '/anggaran/index', NULL, NULL);
INSERT INTO `menu` VALUES (16, 'Set Tahun Ajaran', NULL, '/settahunajaran/index', NULL, NULL);
INSERT INTO `menu` VALUES (18, 'Laporan Akhir', 8, NULL, NULL, NULL);
INSERT INTO `menu` VALUES (21, 'Pengeluaran', NULL, '/pengeluaran/index', NULL, 'cloud-upload');
INSERT INTO `menu` VALUES (24, 'Akun', 25, '/akun/index', NULL, 'folder');
INSERT INTO `menu` VALUES (25, 'Buat Akun', NULL, NULL, NULL, 'bookmark');
INSERT INTO `menu` VALUES (26, 'Sub Akun', 25, '/subakun/index', NULL, 'map');
INSERT INTO `menu` VALUES (27, 'Transaksi Keuangan', NULL, '/transaksi/index', NULL, 'money');
INSERT INTO `menu` VALUES (28, 'Buku Besar', 12, '/bukubesar/index', 2, 'book');
INSERT INTO `menu` VALUES (29, 'Neraca Saldo', 12, '/neracasaldo/index', 5, 'balance-scale');
INSERT INTO `menu` VALUES (30, 'Jurnal Umum', 12, '/jurnalumum/index', 1, 'sticky-note');
INSERT INTO `menu` VALUES (31, 'Laba Rugi', 12, '/labarugi/index', 3, 'file-archive-o');
INSERT INTO `menu` VALUES (32, 'Saldo Awal', NULL, '/saldo-awal/index', NULL, 'bank');
INSERT INTO `menu` VALUES (33, 'Neraca', 12, '/neraca/index', 4, NULL);
INSERT INTO `menu` VALUES (35, 'Kategori Akun', 25, '/kategoriakun/index', NULL, NULL);
INSERT INTO `menu` VALUES (36, 'Merchant', NULL, '/merchant/index', 1, 'building');
INSERT INTO `menu` VALUES (37, 'Piutang', 12, '/piutang/index', 7, 'indent');
INSERT INTO `menu` VALUES (38, 'Hutang', 12, '/hutang/index', 6, 'outdent');

-- ----------------------------
-- Table structure for merchant
-- ----------------------------
DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_merchant` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alamat` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `no_telp` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tanggal_bergabung` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of merchant
-- ----------------------------
INSERT INTO `merchant` VALUES (4, 'PT. Suka Suka', 'jalan suka-suka', 'sukasuka@gmail.com', '06177776622', '2021-12-21 21:43:53');

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration`  (
  `version` varchar(180) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `apply_time` int(11) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', 1570938090);
INSERT INTO `migration` VALUES ('m140506_102106_rbac_init', 1570938099);
INSERT INTO `migration` VALUES ('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1570938099);
INSERT INTO `migration` VALUES ('m180523_151638_rbac_updates_indexes_without_prefix', 1570938100);

-- ----------------------------
-- Table structure for pejabat
-- ----------------------------
DROP TABLE IF EXISTS `pejabat`;
CREATE TABLE `pejabat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pejabat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pejabat
-- ----------------------------
INSERT INTO `pejabat` VALUES (1, 'Rektor');
INSERT INTO `pejabat` VALUES (2, 'Dekan');
INSERT INTO `pejabat` VALUES (3, 'Ka Prodi');
INSERT INTO `pejabat` VALUES (4, 'Yayasan');
INSERT INTO `pejabat` VALUES (5, 'Admin Fakultas');
INSERT INTO `pejabat` VALUES (6, 'Admin Prodi');
INSERT INTO `pejabat` VALUES (7, 'Admin Aplikasi');

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nim` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idprodi` int(11) NOT NULL,
  `angkatan` int(5) NOT NULL,
  `semester` int(2) NOT NULL,
  `tagihan` bigint(20) NOT NULL,
  `biaya_pendidikan` bigint(20) NOT NULL,
  `total_bayar` bigint(20) NOT NULL,
  `tanggal_bayar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_pembayaran_idprodi`(`idprodi`) USING BTREE,
  CONSTRAINT `fk_pembayaran_idprodi` FOREIGN KEY (`idprodi`) REFERENCES `prodi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pendapatan
-- ----------------------------
DROP TABLE IF EXISTS `pendapatan`;
CREATE TABLE `pendapatan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idprodi` int(11) NOT NULL DEFAULT 0,
  `idsubakun` int(11) NOT NULL,
  `no_ref` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `idtahunajaran` int(11) NOT NULL,
  `tanggal` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_pendapatan_idprodi`(`idprodi`) USING BTREE,
  INDEX `fk_pendapatan_idjenispendapatan`(`idsubakun`) USING BTREE,
  INDEX `fk_pendapatan_idtahunajaran`(`idtahunajaran`) USING BTREE,
  CONSTRAINT `fk_pendapatan_idprodi` FOREIGN KEY (`idprodi`) REFERENCES `prodi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pendapatan_idsubakun` FOREIGN KEY (`idsubakun`) REFERENCES `sub_akun` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_pendapatan_idtahunajaran` FOREIGN KEY (`idtahunajaran`) REFERENCES `tahun_ajaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pengeluaran
-- ----------------------------
DROP TABLE IF EXISTS `pengeluaran`;
CREATE TABLE `pengeluaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idprodi` int(11) NOT NULL DEFAULT 0,
  `idsubakun` int(11) NOT NULL,
  `no_ref` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah` bigint(20) NOT NULL,
  `idtahunajaran` int(11) NOT NULL,
  `tanggal` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_pengeluaran_idjenispengeluaran`(`idsubakun`) USING BTREE,
  INDEX `fk_pengeluaran_idtahunajaran`(`idtahunajaran`) USING BTREE,
  INDEX `fk_pengeluaran_idprodi`(`idprodi`) USING BTREE,
  CONSTRAINT `fk_pengeluaran_idprodi` FOREIGN KEY (`idprodi`) REFERENCES `prodi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pengeluaran_idsubakun` FOREIGN KEY (`idsubakun`) REFERENCES `sub_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pengeluaran_idtahunajaran` FOREIGN KEY (`idtahunajaran`) REFERENCES `tahun_ajaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piutang
-- ----------------------------
DROP TABLE IF EXISTS `piutang`;
CREATE TABLE `piutang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsubakun` int(11) NOT NULL,
  `total` bigint(20) NOT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tanggal_bayar` timestamp(0) NULL DEFAULT NULL,
  `tanggal_insert` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_piutang_idsubakun`(`idsubakun`) USING BTREE,
  CONSTRAINT `fk_piutang_idsubakun` FOREIGN KEY (`idsubakun`) REFERENCES `sub_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piutang
-- ----------------------------
INSERT INTO `piutang` VALUES (2, 218, 200000, '2021-12-30', 'Pinjaman ke PT. Makmur Jaya', NULL, '2021-12-30 21:07:41');

-- ----------------------------
-- Table structure for prodi
-- ----------------------------
DROP TABLE IF EXISTS `prodi`;
CREATE TABLE `prodi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idfakultas` int(11) NULL DEFAULT NULL,
  `nama_prodi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode` decimal(2, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_prodi_idfakultas`(`idfakultas`) USING BTREE,
  CONSTRAINT `fk_prodi_idfakultas` FOREIGN KEY (`idfakultas`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prodi
-- ----------------------------
INSERT INTO `prodi` VALUES (1, 1, 'Teknik Informatika', 12);
INSERT INTO `prodi` VALUES (2, 1, 'Sistem Informasi', 13);
INSERT INTO `prodi` VALUES (3, 1, 'Manajemen Informatika', 14);
INSERT INTO `prodi` VALUES (4, 4, 'Perbankan Syariah', 42);
INSERT INTO `prodi` VALUES (5, 4, 'Ekonomi Syariah', 41);
INSERT INTO `prodi` VALUES (6, 4, 'Manajemen', 44);
INSERT INTO `prodi` VALUES (7, 4, 'Akuntansi', 43);
INSERT INTO `prodi` VALUES (8, 5, 'Ilmu Hukum', 61);
INSERT INTO `prodi` VALUES (9, 6, 'Ilmu Hubungan Internasional', 31);
INSERT INTO `prodi` VALUES (10, 6, 'Pendidikan Bahasa Inggris', 32);
INSERT INTO `prodi` VALUES (11, 2, 'Psikologi', 51);
INSERT INTO `prodi` VALUES (12, 7, 'Desain Interior', 23);
INSERT INTO `prodi` VALUES (13, 7, 'Televisi Dan Film', 22);
INSERT INTO `prodi` VALUES (14, 7, 'Desain Komunikasi Visual', 21);
INSERT INTO `prodi` VALUES (15, 1, 'Teknik Industri', 11);
INSERT INTO `prodi` VALUES (16, 8, 'Universitas Potensi Utama', NULL);

-- ----------------------------
-- Table structure for saldo_awal
-- ----------------------------
DROP TABLE IF EXISTS `saldo_awal`;
CREATE TABLE `saldo_awal`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idprodi` int(11) NULL DEFAULT NULL,
  `idsubakun` int(11) NOT NULL,
  `debet` double NULL DEFAULT 0,
  `kredit` double NULL DEFAULT 0,
  `tanggal` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_saldo_awal_idsubakun`(`idsubakun`) USING BTREE,
  INDEX `fk_saldo_awal_idprodi`(`idprodi`) USING BTREE,
  CONSTRAINT `fk_saldo_awal_idprodi` FOREIGN KEY (`idprodi`) REFERENCES `prodi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_saldo_awal_idsubakun` FOREIGN KEY (`idsubakun`) REFERENCES `sub_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for set_tahun_ajaran
-- ----------------------------
DROP TABLE IF EXISTS `set_tahun_ajaran`;
CREATE TABLE `set_tahun_ajaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtahunajaran` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_settahunajaran_idtahunajaran`(`idtahunajaran`) USING BTREE,
  CONSTRAINT `fk_settahunajaran_idtahunajaran` FOREIGN KEY (`idtahunajaran`) REFERENCES `tahun_ajaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of set_tahun_ajaran
-- ----------------------------
INSERT INTO `set_tahun_ajaran` VALUES (1, 1);

-- ----------------------------
-- Table structure for sub_akun
-- ----------------------------
DROP TABLE IF EXISTS `sub_akun`;
CREATE TABLE `sub_akun`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idmerchant` int(11) NOT NULL,
  `idakun` int(11) NOT NULL,
  `idkategoriakun` int(11) NULL DEFAULT NULL,
  `kode_akun` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_sub_akun` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `debet` double NULL DEFAULT 0,
  `kredit` double NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_sub_akun_idakun`(`idakun`) USING BTREE,
  INDEX `fk_sub_akun_idkategoriakun`(`idkategoriakun`) USING BTREE,
  INDEX `fk_sub_akun_idmerchant`(`idmerchant`) USING BTREE,
  CONSTRAINT `fk_sub_akun_idakun` FOREIGN KEY (`idakun`) REFERENCES `akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sub_akun_idkategoriakun` FOREIGN KEY (`idkategoriakun`) REFERENCES `kategori_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_sub_akun_idmerchant` FOREIGN KEY (`idmerchant`) REFERENCES `merchant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 220 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sub_akun
-- ----------------------------
INSERT INTO `sub_akun` VALUES (216, 4, 1, 1, '1001', 'Kas', 0, 0);
INSERT INTO `sub_akun` VALUES (217, 4, 3, NULL, '1002', 'Andi Sanjaya', 0, 0);
INSERT INTO `sub_akun` VALUES (218, 4, 6, NULL, '1003', 'PT. Makmur Jaya', 0, 0);
INSERT INTO `sub_akun` VALUES (219, 4, 2, NULL, '1004', 'PT. Jaya Kusuma', 0, 0);

-- ----------------------------
-- Table structure for tahun_ajaran
-- ----------------------------
DROP TABLE IF EXISTS `tahun_ajaran`;
CREATE TABLE `tahun_ajaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_ajaran` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tahun_ajaran
-- ----------------------------
INSERT INTO `tahun_ajaran` VALUES (1, '2015 - 2016');
INSERT INTO `tahun_ajaran` VALUES (2, '2016 - 2017');

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idmerchant` int(11) NOT NULL,
  `idsubakun` int(11) NOT NULL,
  `idakundebet` int(11) NULL DEFAULT NULL,
  `idakunkredit` int(11) NULL DEFAULT NULL,
  `ke_akun` int(11) NULL DEFAULT NULL,
  `no_ref` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `debet` double(20, 0) NULL DEFAULT NULL,
  `kredit` double(20, 0) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tanggal` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_transaksi_idsubakun`(`idsubakun`) USING BTREE,
  INDEX `fk_transaksi_idakundebet`(`idakundebet`) USING BTREE,
  INDEX `fk_transaksi_idakunkredit`(`idakunkredit`) USING BTREE,
  CONSTRAINT `fk_transaksi_idakundebet` FOREIGN KEY (`idakundebet`) REFERENCES `sub_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_transaksi_idakunkredit` FOREIGN KEY (`idakunkredit`) REFERENCES `sub_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_transaksi_idsubakun` FOREIGN KEY (`idsubakun`) REFERENCES `sub_akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 357 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES (334, 4, 217, 217, NULL, 216, '20220104140919', NULL, 20000000, 'Modal dari Andi Sanjaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (335, 4, 216, NULL, 216, 217, '20220104140919', 20000000, NULL, 'Modal dari Andi Sanjaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (336, 4, 216, 216, NULL, 218, '20220104141022', NULL, 500000, 'Pinjaman PT Makmur Jaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (337, 4, 218, NULL, 218, 216, '20220104141022', 500000, NULL, 'Pinjaman PT Makmur Jaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (338, 4, 218, 218, NULL, 216, '20220104144222', NULL, 200000, 'Pelunasan Pinjaman PT.Makmur Jaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (339, 4, 216, NULL, 216, 218, '20220104144222', 200000, NULL, 'Pelunasan Pinjaman PT.Makmur Jaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (340, 4, 218, 218, NULL, 216, '20220104145303', NULL, 200000, 'Sisa Pelunasan PT. Makmur Jaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (341, 4, 216, NULL, 216, 218, '20220104145303', 200000, NULL, 'Sisa Pelunasan PT. Makmur Jaya', '2022-01-04');
INSERT INTO `transaksi` VALUES (342, 4, 218, 218, NULL, 216, '20220104145345', NULL, 100000, 'Sisa Pelunasan', '2022-01-04');
INSERT INTO `transaksi` VALUES (343, 4, 216, NULL, 216, 218, '20220104145345', 100000, NULL, 'Sisa Pelunasan', '2022-01-04');
INSERT INTO `transaksi` VALUES (344, 4, 219, 219, NULL, 216, '20220104154414', NULL, 1000000, 'Hutang ke PT. Jaya Kusuma', '2022-01-04');
INSERT INTO `transaksi` VALUES (345, 4, 216, NULL, 216, 219, '20220104154414', 1000000, NULL, 'Hutang ke PT. Jaya Kusuma', '2022-01-04');
INSERT INTO `transaksi` VALUES (353, 4, 216, 216, NULL, 219, '20220104155058', NULL, 500000, 'Pembayaran Hutang', '2022-01-04');
INSERT INTO `transaksi` VALUES (354, 4, 219, NULL, 219, 216, '20220104155058', 500000, NULL, 'Pembayaran Hutang', '2022-01-04');
INSERT INTO `transaksi` VALUES (355, 4, 216, 216, NULL, 219, '20220104155723', NULL, 500000, 'Pembayaran Hutang Lagi', '2022-01-04');
INSERT INTO `transaksi` VALUES (356, 4, 219, NULL, 219, 216, '20220104155723', 500000, NULL, 'Pembayaran Hutang Lagi', '2022-01-04');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpejabat` int(11) NOT NULL DEFAULT 10,
  `idmerchant` int(11) NULL DEFAULT NULL,
  `idprodi` int(11) NULL DEFAULT 0,
  `nama` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  UNIQUE INDEX `password_reset_token`(`password_reset_token`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 8, 0, 0, '', 'admin', 'h0pAroLgO3MwuEN2X2ufdfXoeOfyzm77', '$2y$13$uU6pldS6JjFvl4IMOisyDeTWKzmnv4WFGn3FgVjnBAD3sVCPVX18C', NULL, 'admin@gmail.com', 10, 1561701675, 1598928548, NULL, NULL);
INSERT INTO `user` VALUES (13, 2, 0, 0, 'Rika Rosnelly', 'rika', 'mFLnP-l19FGZ_64zG6Dhqk3Em7umsfSu', '$2y$13$MvA0ChGAfIaU5P9femzFN.wrh7TFj8.wxc9QmvxJkaQN9umg3a7g2', NULL, 'rika@gmail.com', 10, 1570964681, 1570965160, NULL, NULL);
INSERT INTO `user` VALUES (23, 5, 0, 0, 'Linda', 'linda', 'tRubkf4aLk_AQCQvXP1zGxmbcJ6TW2wg', '$2y$13$jwxnHc/PE70XkfsXYbTtQePXrq1FA/57/xSBW/Kq0NGQUjEt/tjPu', NULL, 'linda@gmail.com', 10, 1573890891, 1573890891, NULL, NULL);
INSERT INTO `user` VALUES (27, 8, 0, 0, 'tes', 'tes', 'uKswxkV4Gk5tfZi6YDNxfRrvfoocs9lt', '$2y$13$F86L5fKNMYJDhR8ezFcTs.bxIGRfQdI4ZhowcYg4aDPLYW3bsLiMS', NULL, 'tes', 10, 1573978064, 1573978064, NULL, NULL);
INSERT INTO `user` VALUES (28, 4, 0, 0, 'yayasan', 'yayasan', 'vJtizAYl4Qw0giFfqWxXeNutYlpz0H8J', '$2y$13$0sN.XXTHP4c5lOnDAkipHuFC5Gt66pNKmh8TWlyQD7FPVxlw8Z/2a', NULL, 'yayasan@gamil.com', 10, 1598926148, 1598926148, NULL, NULL);
INSERT INTO `user` VALUES (29, 10, 3, 3, 'Admin Manajemen Informatika', 'prodi_manajemen_informatika', 'hwftwTJbamF1xCDF39K75eaNn3Bxdm1s', '$2y$13$ZAGQ9elr9Uhpc.0.yDa8KeefsQeiWcbntx1qTAkCZYDUnAh5B6.vy', NULL, 'prodi_mi@gmail.com', 10, 1602078713, 1602080289, NULL, NULL);
INSERT INTO `user` VALUES (30, 6, 2, 2, 'Admin Sistem Informasi', 'prodi_sistem_informasi', 'Ie4F0GyyaikK1ORbUxBQVLzQNwDY66lY', '$2y$13$1j0uaulUYHZ4V0IsDK1ObOMPyrm71dInR3UaK5ut783Z2wp8dNeI6', NULL, 'prodi_si@gmail.com', 10, 1602078761, 1602080321, NULL, NULL);
INSERT INTO `user` VALUES (31, 6, 15, 15, 'Admin Teknik Industri', 'prodi_teknik_industri', 'hc01BmREmsxB4GwUBHG4LQveYx56HkFy', '$2y$13$IBBfiUCIUQacguFcNLP2iemzKDuGYfFJnD2.GipSmvU8.2GvTSFiu', NULL, 'prodi_tind@gmail.com', 10, 1602078842, 1602080348, NULL, NULL);
INSERT INTO `user` VALUES (32, 6, 1, 1, 'Admin Teknik Informatika', 'prodi_teknik_informatika', 'WvJtYyoRU_I4mFu8OEsaxIGMPMldPm2i', '$2y$13$A2X/sNyRf6Z3izGOP.JEWuT.DjVLkjIwKtYS62I0IK5dUx7OXMsde', NULL, 'prodi_ti@gmail.com', 10, 1602078915, 1602080371, NULL, NULL);
INSERT INTO `user` VALUES (33, 6, 11, 11, 'Admin Psikologi', 'prodi_psikologi', 'ie7-u9bm9g7uv2dlm9dpcg5OFi-yIZ6S', '$2y$13$8qtxM2f.GCZn9fgMZbKFwO04XLVoSqJcAQpU5Epz127pn/INLsFaq', NULL, 'prodi_psi@gmail.com', 10, 1602078962, 1602080399, NULL, NULL);
INSERT INTO `user` VALUES (34, 6, 7, 7, 'Admin Akuntansi', 'prodi_akuntansi', 'oj4_8mrMdLPOgFT4gRx5p49cfECNCAwu', '$2y$13$SlHdVDoTVAy3hiynk2DBOeStCmpkHZywemPKxaAvJMopEBWppEex6', NULL, 'prodi_ak@gmail.com', 10, 1602079010, 1602080449, NULL, NULL);
INSERT INTO `user` VALUES (35, 6, 5, 5, 'Admin Ekonomi Syariah', 'prodi_ekonomi_syariah', 'ruY6Xc1v7e1SwQMgDjwJE39BiGUPqU1b', '$2y$13$iuq87XrfHi/YOG7/49bH/.pG7tKZreusmvFya3jPlXcrt2jWTjAPO', NULL, 'prodi_es@gmail.com', 10, 1602079083, 1602080472, NULL, NULL);
INSERT INTO `user` VALUES (36, 6, 6, 6, 'Admin Manajemen', 'prodi_manajemen', 'i8dmVdnDk0hwzy5Zew4j0QGoerHHrWqX', '$2y$13$s2y75eLLnOhNHQTJgJjewODvs1ZWuKJe8qYLPmbG6LZ1e9S7VgpbW', NULL, 'prodi_manajemen@gmail.com', 10, 1602079163, 1602080494, NULL, NULL);
INSERT INTO `user` VALUES (37, 6, 4, 4, 'Admin Perbankan Syariah', 'prodi_perbankan_syariah', 'LyjtuJ5Bs0zWqVb7uJxz67twFFggHKAM', '$2y$13$AufEzQuNCECGG3bf97bec.A6OO.8Lj/pLqjbBe.m.U9VTZSEwdm2S', NULL, 'prodi_perbankan_syariah@gmail.com', 10, 1602079236, 1602080519, NULL, NULL);
INSERT INTO `user` VALUES (38, 6, 8, 8, 'Admin Ilmu Hukum', 'prodi_ilmu_hukum', 'uwQT32xXl2pUt-wM93Rx7is36ToGA4Zp', '$2y$13$ruiIL12PLf.tIojESpeEfeC1Q6L8fZbGIrmAuKjv9OY4YYIaMdk8m', NULL, 'prodi_ilmu_hukum@gmail.com', 10, 1602079290, 1602080539, NULL, NULL);
INSERT INTO `user` VALUES (39, 6, 9, 9, 'Admin Ilmu Hubungan Internasional', 'prodi_hubungan_international', 'fytSXuFat_cE5QTAFHrrm_8ESVd8DyhE', '$2y$13$bK1xnhRpshZsuxJlp/KGB.P62zTpAyZIB/WOfddvKGqX9nN/jqZyu', NULL, 'prodi_hubungan_international@gmail.com', 10, 1602079349, 1602080560, NULL, NULL);
INSERT INTO `user` VALUES (40, 6, 10, 10, 'Admin Bahasa Inggris', 'prodi_bahasa_inggris', 'OsKO20iQ2zm15nLf53Zh1FwyAI7RZ85r', '$2y$13$QG2zaGgm0gf4cfU0M790tOaiJHcD3EyiNcoxOCUbg/Z/nFN1rakRG', NULL, 'prodi_bahasa_inggris@gmail.com', 10, 1602079391, 1602080587, NULL, NULL);
INSERT INTO `user` VALUES (41, 6, 12, 12, 'Admin Desain Interior', 'prodi_desain_interior', 'kkqRCtwAxqHi8qRRexekFMgUbX5Z0ZNb', '$2y$13$Nj6fqJWiafAeoaHYSl.Wh.EUcDowEOXJQ8z8CIVr4tX/qY6vTIqpS', NULL, 'prodi_desain_interior@gmail.com', 10, 1602079437, 1602080607, NULL, NULL);
INSERT INTO `user` VALUES (42, 6, 14, 14, 'Admin Desain Komunikasi Visual', 'prodi_desain_komunikasi_visual', 'JcVHXQ5MNNYv4QaRRHnbO144Oy9xjK7n', '$2y$13$Y18xgoYquUGlz/ZuA6qevOmKixOsiDRBr1JbkCDLDasIivCorcj8q', NULL, 'prodi_desain_komunikasi_visual@gmail.com', 10, 1602079490, 1602080630, NULL, NULL);
INSERT INTO `user` VALUES (43, 6, 13, 13, 'Admin Televisi Film', 'prodi_televisi_film', 'TOJ8e_qgxIn58-Iqp4XWGpdcckhWUXKV', '$2y$13$aA6tp.K4xmWuB5zyK2z1OeXUXKjJx.HAFy8YZwWukzzaZkprhfTbm', NULL, 'prodi_televisi_film@gmail.com', 10, 1602079535, 1602080655, NULL, NULL);
INSERT INTO `user` VALUES (44, 6, 16, 16, 'Admin Universitas', 'universitas', '8woa4WuWRSvL_gJOxT0Um0zfHUF0HgtT', '$2y$13$E74qXxcwoTGBTQSRu4fFcevVyUEVceBN3BJttrRxbcTqUJxSz1sBa', NULL, 'universitas@gmail.com', 10, 1602838058, 1602838428, NULL, NULL);
INSERT INTO `user` VALUES (47, 10, 4, 0, 'PT. Suka Suka', 'sukasuka@gmail.com', 'UM_dNlH5ebVaef0rAxepsN4qRbWKCRi8', '$2y$13$BTn9/F4131er56S7bip21.j7cq6M9qptRFmSYJk9PfVlHMlg6jNQO', NULL, 'sukasuka@gmail.com', 10, 1640097834, 1642431002, NULL, 'Merchant');

-- ----------------------------
-- View structure for v_bukubesar
-- ----------------------------
DROP VIEW IF EXISTS `v_bukubesar`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_bukubesar` AS SELECT
	`tr`.`idmerchant` AS `idmerchant`,
	`tr`.`tanggal` AS `tanggal`,
	tr.idakun,
	concat('(',tr.akun, ') ', `tr`.`nama_akun`) AS `nama_sub_akun`,
	`tr`.`nama_sub_akun` AS `nama_akun`,
	tr.no_ref,
	`tr`.`debet` AS `debet`,
	`tr`.`kredit` AS `kredit`,
	(
	SELECT
		sum( ifnull( `tra`.`debet`, 0 ) - ifnull( `tra`.`kredit`, 0 ) ) 
	FROM
		`v_transaksi` `tra` 
	WHERE
		`tra`.`id` <= `tr`.`id` 
		AND `tra`.`idsubakun` = `tr`.`idsubakun` 
	) AS `saldo` 
FROM
	`v_transaksi` AS `tr` 
ORDER BY
	tr.idakun ASC,
	tr.id asc,
	`tr`.`tanggal` ASC ;

-- ----------------------------
-- View structure for v_jurnal
-- ----------------------------
DROP VIEW IF EXISTS `v_jurnal`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_jurnal` AS SELECT `tr`.`tanggal` AS `tanggal`, `sa`.`nama_sub_akun` AS `nama_sub_akun`, `sk`.`nama_sub_akun` AS `nama_akun`, `tr`.`debet` AS `debet`, `tr`.`kredit` AS `kredit`, `tr`.`ke_akun` AS `ke_akun`, (select sum(ifnull(`tra`.`debet`,0) - ifnull(`tra`.`kredit`,0)) from `transaksi` `tra` where `tra`.`id` <= `tr`.`id` and `tra`.`ke_akun` = `tr`.`ke_akun`) AS `saldo` FROM ((`transaksi` `tr` join `sub_akun` `sk` on(`tr`.`idsubakun` = `sk`.`id`)) join `sub_akun` `sa` on(`tr`.`ke_akun` = `sa`.`id`)) ORDER BY `tr`.`ke_akun` ASC, `tr`.`tanggal` ASC ; ;

-- ----------------------------
-- View structure for v_neraca
-- ----------------------------
DROP VIEW IF EXISTS `v_neraca`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_neraca` AS SELECT `tr`.`tanggal` AS `tanggal`, `sa`.`nama_sub_akun` AS `nama_sub_akun`, (select sum(ifnull(`tra`.`debet`,0) - ifnull(`tra`.`kredit`,0)) from `transaksi` `tra` where `tra`.`ke_akun` = `tr`.`ke_akun`) AS `saldo` FROM (`transaksi` `tr` join `sub_akun` `sa` on(`tr`.`ke_akun` = `sa`.`id`)) ORDER BY `tr`.`ke_akun` ASC ; ;

-- ----------------------------
-- View structure for v_transaksi
-- ----------------------------
DROP VIEW IF EXISTS `v_transaksi`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_transaksi` AS SELECT
	`tr`.`id` AS `id`,
	`tr`.`idmerchant` AS `idmerchant`,
	`tr`.`tanggal` AS `tanggal`,
	`sa`.`nama_sub_akun` AS `nama_sub_akun`,
	`sk`.`nama_sub_akun` AS `nama_akun`,
	`tr`.`debet` AS `debet`,
	`tr`.`kredit` AS `kredit`,
	`tr`.`ke_akun` AS `ke_akun`,
	`tr`.`idsubakun` AS `idsubakun`,
	ak.nama_akun as akun,
	tr.no_ref,
	tsa.idakun
FROM
	(
		( `transaksi` `tr` JOIN `sub_akun` `sk` ON ( `tr`.`idsubakun` = `sk`.`id` ) )
	JOIN `sub_akun` `sa` ON ( `tr`.`ke_akun` = `sa`.`id` ) 
	JOIN `sub_akun` `tsa` ON ( `tr`.`idsubakun` = `tsa`.`id` )
	join akun ak on tsa.idakun = ak.id
	
	) ;

SET FOREIGN_KEY_CHECKS = 1;
