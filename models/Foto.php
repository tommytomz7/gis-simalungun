<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foto".
 *
 * @property int $id
 * @property int $idinformasi
 * @property string $nama_foto
 */
class Foto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idinformasi', 'nama_foto'], 'required'],
            [['idinformasi'], 'integer'],
            [['nama_foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idinformasi' => 'Idinformasi',
            'nama_foto' => 'Nama Foto',
        ];
    }

    public function getInformasi()
    {
        return $this->hasOne(Informasi::className(), ['id' => 'idinformasi']);
    }

}
