<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "informasi".
 *
 * @property int $id
 * @property int $idkategori
 * @property string $judul
 * @property string $deskripsi
 * @property float $lat
 * @property float $long
 *
 * @property Kategori $idkategori0
 */
class Informasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'informasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idkategori', 'judul', 'deskripsi', 'lat', 'long'], 'safe'],
            [['idkategori'], 'integer'],
            [['deskripsi'], 'string'],
            [['lat', 'long'], 'number'],
            [['judul'], 'string', 'max' => 100],
            [['idkategori'], 'exist', 'skipOnError' => true, 'targetClass' => Kategori::className(), 'targetAttribute' => ['idkategori' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idkategori' => 'Kategori',
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'lat' => 'Lat',
            'long' => 'Long',
        ];
    }

    /**
     * Gets query for [[Idkategori0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKategori()
    {
        return $this->hasOne(Kategori::className(), ['id' => 'idkategori']);
    }
}
