<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property string $judul
 * @property string $url
 * @property string|null $url_image
 * @property string|null $deskripsi
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul', 'url'], 'required'],
            [['url', 'deskripsi'], 'string'],
            [['judul', 'url_image'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'url' => 'Url',
            'url_image' => 'Url Image',
            'deskripsi' => 'Deskripsi',
        ];
    }
}
